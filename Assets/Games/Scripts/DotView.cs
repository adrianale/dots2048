﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DotView : MonoBehaviour, IPointerDownHandler, IPointerEnterHandler, IPointerExitHandler
{
    static Color defaultColor = new Color(1, 1, 1, 0);
    static Color selectedColor = new Color(1, 1, 0, 0.4f);
    
    [SerializeField] Image imageBG;
    [SerializeField] Image imageDot;
    [SerializeField] TextMeshProUGUI text;

    [Header("Color")]
    [SerializeField] Color[] colors;

    GameManager gameManager;
    int index;
    int value;
    bool isSelected;

    public void Init(GameManager inGameManager, int inIndex)
    {
        gameManager = inGameManager;
        index = inIndex;
        SetValue(1);

        isSelected = false;
    }
    
    public void SetValue(int val)
    {
        value = val;
        int visualVal = (int)Math.Pow(2, val);
        text.SetText(visualVal.ToString());
    }

    public void ResetState()
    {
        imageBG.color = defaultColor;
        isSelected = false;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        gameManager.StartMerge(index);
        imageBG.color = selectedColor;
    }
    
    public void OnPointerEnter(PointerEventData eventData)
    {
        if(gameManager.isMerging && gameManager.selectedVal == value && gameManager.IsAdjacent(index))
        {
            isSelected = !isSelected;
            imageBG.color = isSelected ? selectedColor : defaultColor;
            if(isSelected)
            {
                gameManager.AddDot(index);
            }
            else
            {
                gameManager.RemoveDot(index);
            }
            
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        
    }
    
}
