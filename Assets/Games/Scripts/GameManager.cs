﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.SymbolStore;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class GameManager : MonoBehaviour
{
    [Header("Game Board")]
    [SerializeField] Transform containerDotView;
    [SerializeField] DotView prefabDotView;

    [Header("Config")]
    [SerializeField] int width = 5;
    [SerializeField] int height = 5;
    
    int[] grid;
    int gridCount;
    List<DotView> dotViews;

    public bool isMerging;
    List<int> selectedDots;
    public int selectedVal;

    List<int> cols;
    
    // Start is called before the first frame update
    void Start()
    {
        gridCount = width * height;
        grid = new int[gridCount];
        dotViews = new List<DotView>();
        for(int i = 0; i < height; i++)
        {
            for(int j = 0; j < width; j++)
            {
                int index = GetIndex(j, i);
                grid[index] = 1;
                
                DotView dot = Instantiate(prefabDotView, containerDotView);
                dot.Init(this, index);
                dotViews.Add(dot);
            }
        }
        
        selectedDots = new List<int>();
        cols = new List<int>();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetMouseButtonUp(0))
        {
            isMerging = false;
            Merge();
        }
    }

    int GetIndex(int col, int row)
    {
        int index = row * width + col;
        return index;
    }

    int GetRow(int index)
    {
        return index / width;
    }
    
    int GetCol(int index, int row)
    {
        return index - (row * width);
    }

    public bool IsAdjacent(int index)
    {
        int count = selectedDots.Count;
        int lastIndex = selectedDots[count - 1];

        bool adjacent = index == (lastIndex - 1) || index == (lastIndex + 1) || index == (lastIndex - width) || index == (lastIndex + width);
        return adjacent;
    }
    
    public void StartMerge(int index)
    {
        selectedDots.Clear();
        ResetDotsState();
        
        selectedDots.Add(index);
        selectedVal = grid[index];
        isMerging = true;
    }

    public void AddDot(int index)
    {
        if(!selectedDots.Contains(index))
        {
            selectedDots.Add(index);
        }
    }

    public void RemoveDot(int index)
    {
        if(selectedDots.Contains(index))
        {
            selectedDots.Remove(index);
        }
    }

    public void Merge()
    {
        int count = selectedDots.Count;
        if(count > 1)
        {
            int lastIndex = selectedDots[count - 1];

            int newVal = selectedVal * count;
            grid[lastIndex] = newVal;
            dotViews[lastIndex].SetValue(newVal);

            // Remove last dots
            selectedDots.Remove(lastIndex);
            count -= 1;

            // Refresh values
            cols.Clear();
            for(int i = 0; i < count; i++)
            {
                int index = selectedDots[i];
                int row = GetRow(index);
                int col = GetCol(index, row);
                if(!cols.Contains(col))
                {
                    cols.Add(col);
                }
            }

            int lastRow = GetRow(lastIndex);
            count = cols.Count;
            for(int i = 0; i < count; i++)
            {
                UpdateGridColumn(cols[i], lastRow, lastIndex);
            }
        }

        selectedVal = 0;
        ResetDotsState();
    }

    public void ResetDotsState()
    {
        for(int i = 0; i < gridCount; i++)
        {
            dotViews[i].ResetState();
        }
    }

    void UpdateGridColumn(int col, int row, int excludeIndex)
    {
        int newValue = 1;
        for(int i = row; i < height; i++)
        {
            int currIndex = GetIndex(col, i);
            if(currIndex != excludeIndex)
            {
                int topIndex = currIndex + width;
                if(topIndex > gridCount - 1)
                {
                    newValue = Random.Range(1, 5);
                }
                else
                {
                    newValue = grid[topIndex];
                }

                grid[currIndex] = newValue;
                dotViews[currIndex].SetValue(newValue);
                
                Debug.Log(string.Format("[{0},{1}]", col, i));
            }
        }
    }

    int GetMaxValueOnGrid()
    {
        int max = 0;
        for(int i = 0; i < gridCount; i++)
        {
            if(max < grid[i])
            {
                max = grid[i];
            }
        }

        return max;
    }
    
}
